package driver;

import devices.Device;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class TCPDriverInstance implements Runnable {

	private Socket clientSocket;
	private Device device;


	public TCPDriverInstance(Socket clientSocket, Device console) {
		this.clientSocket = clientSocket;
		this.device = console;
	}


	public void run() {

		try {
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			while(true) {
				String readLine = in.readLine();

				if (readLine != null) {
					device.interpreta(readLine, out);
					out.flush();
				}
			}
		} catch (Exception ex) {
			//TODO logs
		}

	}
}
