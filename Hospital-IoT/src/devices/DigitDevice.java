package devices;

public class DigitDevice extends IODevice<Boolean> {
    
    public DigitDevice(String name) {
        super(name);
        this.setValue(Boolean.FALSE);
    }

    @Override
    public void parserAndSetValue(String valueString) {
      this.setValue(Boolean.parseBoolean(valueString));
    }
}
