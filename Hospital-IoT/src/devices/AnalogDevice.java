package devices;

public class AnalogDevice extends IODevice<Integer> {
    
    public AnalogDevice(String name) {
        super(name);
        this.setValue(new Integer(0));
    }

    @Override
    public void parserAndSetValue(String valueString) {
        Integer parseValue = Integer.parseInt(valueString);
        this.setValue(parseValue);
    }
    
}
